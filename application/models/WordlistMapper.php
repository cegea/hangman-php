<?php

class Application_Model_WordlistMapper
{

	protected $_dbTable;

	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}

	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Wordlist');
		}
		return $this->_dbTable;
	}

	public function save(Application_Model_Wordlist $word)
	{
		$lastInsertId = NULL;
		$data = array(
				'word' => $word->getWord(),
		);

		if (null === ($id = $word->getId())) {
			unset($data['id']);
			$lastInsertId = $this->getDbTable()->insert($data);
			return $lastInsertId;
		} else {
			$lastInsertId= $this->getDbTable()->update($data, array('id = ?' => $id));
			return $lastInsertId;
		}
	}

	public function find($id, Application_Model_Wordlist $word)
	{
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$word->setId($row->id)
		->setWord($row->word);
	}

	public function fetchAll()
	{
		$resultSet = $this->getDbTable()->fetchAll();

		return $resultSet;
	}
}

