<?php

class Application_Form_AddElement extends Zend_Form
{

    public function init()
    {
    	//For styling
        $this->setAttribs(array('class'=>'element-form'));
        
        //Where and how to send the data
    	$this->setAction('/hangman/add-word');
    	$this->setMethod('post');
    	
    	//Text Input
    	$word = new Zend_Form_Element_Text('word');
    	$word->setLabel('Enter the new word here :')
		    	->setAttrib('size', 35)
		    	//A validator with a customized error message
		    	->setRequired(true)
		    	->addErrorMessage('\' word \' is required and can\'t be empty.');

    	//Attach the Text Input to the form	
    	$this->addElement($word);
    	
    	//Submit button
    	$this->addElement('submit', 'submit', array('ignore' => true,'label' => 'Submit'));
    }

}

