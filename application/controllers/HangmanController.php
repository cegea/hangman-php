<?php

class HangmanController extends Zend_Controller_Action
{
	private $alpha;
	private $wordList;

    public function init()
    {
    	// contains your alphabet
    	$this->alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	
        // list of words to guess
        $wordListMapper = new Application_Model_WordlistMapper();
        $this->wordList = $wordListMapper->fetchAll();
        
        // custom action Helper
        $this->wordList = $this->_helper->rowsetToArray->getArray($this->wordList);
    }

    public function indexAction()
    {

        
    }

    public function addWordAction()
    {          	
    	$form = new Application_Form_AddElement();
    	
    	// make the form available for the view
    	$this->view->addContentForm = $form;
    	
        if ($this->_request->isPost()) {
        	$formData = $this->_request->getPost();        	
        	
        	// a word is required
        	if ($form->isValid($formData)) {
        		$lastInsertId = NULL;
        		
        		// save data
        		$word = new Application_Model_Wordlist();
        		$wordMapper = new Application_Model_WordlistMapper();
				$word->setWord($formData['word']);
				$lastInsertId = $wordMapper->save($word);
				
        		// redirect to word-added page
				$redirectionurl = $this->view
									->url(array('controller' => 'hangman',
													'action'=>'word-added',
													'lastInsertId'=>$lastInsertId
											),
											'default', true
										);
        		
				$this->_redirect($redirectionurl);
        	}  
        	else{
        		$this->view->addContentForm = $form;
        	}
        }
        $this->view->addContentForm = $form;
    }

    public function playAction()
    {	 
    	if($this->getRequest()->get('n')){
    		$n = (integer) $this->getRequest()->get('n');
    	}
    	
    	if($this->getRequest()->get('letters')){
    		$letters = strtoupper($this->getRequest()->get('letters'));
    	}
    	 
    	if(!isset($letters)){
    		$letters = "";
    	}
    	    	 
    	// maximum number of wrong
    	$max = 6;
    	 
    	// construct word list
    	$words = $this->wordList;
    	
    	// seed the random number generator
    	srand ((double)microtime()*1000000);
    	
    	// pick a word from the list
    	if(!isset($n)){
    		$n = rand(1,count($words)) - 1;
    	}
    	
    	// we simplify the game and researches
    	$word = strtoupper(trim($words[$n]));
    	
    	// if the user tries to inject a wrong parameter, redirect
    	if($n > count($words)-1){
    		$redirectionurl = $this->view->url(array('controller' => 'hangman','action'=>'play'),'default', true	);
    		$this->_redirect($redirectionurl);
    	}
   	 	
    	// construct the answer
    	$response = $this->_helper->ConstructResponse->makeResponse($letters, $word);
    	$this->view->response = $response->response;
    	
    	// construct the letter panel, we use the built-in url helper
    	$url = $this->_helper->url('play','Hangman');
    	$letterPanel = $this->_helper->ConstructLetterPanel
    								->makeLetterPanel($letters, $this->alpha, $word, $url, $n);
    	$this->view->letterPanel = $letterPanel->links;
    			
    	// some informations to the user, the tries left
    	$this->view->nwrong = $letterPanel->wrong;
    	$this->view->max = $max;
    	
		// if no more tries left the game is over
    	if($letterPanel->wrong >= $max){
    		// init the picture and counter
    		$this->view->nwrong = 6;
    		
	    	//choose next word
	    	$n++;		 
	    	
	    	if($n > (count($words) - 1)){
	    		$n=0;
	    	}

	 		// Show the word
	    	$this->view->word = $word;
	    	
	    	// invite the user to play again
	    	$url = $this->view->url(
				    				array(
				    						'controller' => 'Hangman',
				    						'action'=>'play',
				    						'n'=> $n,
				    				),
				    				'default', true);
	    	
	    	// we disable $this->view->letterPanel
	    	$letterPanel = $this->_helper->DisableLetterPanel
	    								->makeDisabledLetterPanel($letters, $this->alpha, $word);
	    	$this->view->letterPanel = $letterPanel->links;
	    	
	    	// no more tries left
	    	$this->view->wrongLeft = 0;
	    	
	    	// invite the user to play again
	    	$this->view->playAgain = $url;
    	}	 
    	elseif($response->win){
    		// next word
    		$n++;
    		
    		if($n > (count($words)-1)){
    			$n=0;
    		}
    		
    		// indicates the correct word
    		$this->view->win = $response->win;
    		
	    	$url = $this->view->url(
				    				array(
				    						'controller' => 'Hangman',
				    						'action'=>'play',
				    						'n'=> $n,
				    				),
				    				'default', true);
	    	
	    	// we disable $this->view->letterPanel
	    	$letterPanel = $this->_helper->DisableLetterPanel
	    								->makeDisabledLetterPanel($letters, $this->alpha, $word);
	    	$this->view->letterPanel = $letterPanel->links;
	    	
	    	// no more tries left
	    	$this->view->wrongLeft = 0;
	    	
	    	// invite the user to play again
	    	$this->view->playAgain = $url;
    	}
    	else{
    		$this->view->wrongLeft = $max - $letterPanel->wrong;
    	}
    }
    
    public function wordAddedAction()
    {
    	//to check the last instert in the global list
    	$this->view->lastInsertId = $this->_request->get('lastInsertId');
    	
    	$wordMapper = new Application_Model_WordlistMapper();
    	//the global list
    	$this->view->words = $wordMapper->fetchAll();
    }


}







