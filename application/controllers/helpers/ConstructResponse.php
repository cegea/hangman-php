<?php
class Zend_Controller_Action_Helper_ConstructResponse extends Zend_Controller_Action_Helper_Abstract{
	
	public function makeResponse($letters, $word){
		
	    $i = 0;
	    $response['win'] = false;
	    $response['response'] = Null;
    	 
    	// construct the response
    	while($i < strlen($word)){
    		// search if each caracter in the string is in $letters (letters sent)
    		if(strstr($letters, $word[$i])){
    			 $response['response'] .= $word[$i];
    		}
    		else{		 
    			$response['response'] .= "_&nbsp";
    		}
    		$i++;
    	}
    	// if the response is good the game has to be ended
    	if(strstr(str_replace("_&nbsp"," ",$response['response']),$word)){
    		
    		$response['win'] = true;
    	}
    	
    	return (object) $response;
	}

}


