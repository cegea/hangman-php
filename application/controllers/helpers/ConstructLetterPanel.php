<?php
class Zend_Controller_Action_Helper_ConstructLetterPanel extends Zend_Controller_Action_Helper_Abstract{
	
	public function makeLetterPanel($letters, $alpha, $word,$url,$n){
		
		// construct the available letters the user can select
		$i = 0;
		$lenAlpha = strlen($alpha);
		$letterPanel['wrong'] = 0;
		$letterPanel['links'] = null;
		
		while($i < $lenAlpha){
			//check the letters being sent
			if(strstr($letters, $alpha[$i])){
				//check if the $alpha letter being checked is in the $word
				if(strstr($word, $alpha[$i])){
					$letterPanel['links'] .= $alpha[$i]."\n";
				}
				else{
					$letterPanel['links'] .= "\n<font color=\"red\">".$alpha[$i]."</font>\n";
					$letterPanel['wrong']++;
				}
			}
			else{	
				//the remaining letters available
				$letterPanel['links'] .= "\n<a href='"."$url/letters/".$alpha[$i].$letters."/n/$n"."'>".$alpha[$i]."</a> ";
			}
			$i++;
		}
		
		return (object) $letterPanel;
	}
		


}