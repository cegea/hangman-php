<?php
class Zend_Controller_Action_Helper_DisableLetterPanel extends Zend_Controller_Action_Helper_Abstract{
	
	public function makeDisabledLetterPanel($letters, $alpha, $word){
		
		// construct the letter panel
		$i = 0;
		$lenAlpha = strlen($alpha);
		$DisabledletterPanel['links'] = null;
		
		while($i < $lenAlpha){
			//check the letters being sent
			if(strstr($letters, $alpha[$i])){	
				//check if the $alpha letter being checked is in the $word
				if(strstr($word, $alpha[$i])){
					$DisabledletterPanel['links'] .= "\n".$alpha[$i]."\n";
				}
				else{
					$DisabledletterPanel['links'] .= "\n<font color=\"red\">".$alpha[$i]."</font>\n";
				}
			}
			else{
				// show the letters played among the others
				$DisabledletterPanel['links'] .= "\n".$alpha[$i];
			}
			$i++;
		}
		
		return (object) $DisabledletterPanel;
	}
		


}