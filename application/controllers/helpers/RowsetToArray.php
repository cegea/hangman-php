<?php
class Zend_Controller_Action_Helper_RowsetToArray extends Zend_Controller_Action_Helper_Abstract{
	
	public function getArray($resultSet){
		
		foreach ($resultSet as $k => $v){
			$array[] = strtoupper($v['word']);
		}
		
		return $array;
	}

}

